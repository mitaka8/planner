function $e(q) {
    if (q[0] === "#") {
        return document.getElementById(q.slice(1));
    } else if (q[0] === ".") {
        return document.getElementsByClassName(q.slice(1));
    } else {
        return document.querySelectorAll(q);
    }
}

function $h(t,a,c) {
    if (t[0] === ".") {
        a['class'] = t.slice(1)
        t = "div";
    }
    let e = document.createElement(t);

    for (const attr in a) {
        if (!Object.prototype.hasOwnProperty.call(a,attr)) continue;
        if (attr[0] === "@") {
            e.addEventListener(attr.slice(1), a[attr](e));
        } else {
            e.setAttribute(attr,a[attr]);
        }
    }

    c.forEach((child) => {
        if (typeof child === "string") {
            child = document.createTextNode(child);
        }
        e.appendChild(child);
    });

    return e;
}


document.addEventListener('readystatechange', () => {
    if (document.readyState !== "interactive") return;
    let inputH = $e("#height-input");
    let inputW = $e("#width-input");
    let inputN = $e("#name-input");
    let button = $e("#create-element");
    let field = $e("#field")
    let inputI = $e("#import")
    let exportB = $e("#export-button");
    let importB = $e("#import-button");
    let zindexCounter = 1000;
    button.addEventListener('click', () => createElement());

    importB.addEventListener("click", () => {
        localStorage.setItem("list", inputI.value);
        location.reload();
    });

    exportB.addEventListener("click", () => {

        alert(JSON.stringify(list));

    })

    let list = [];
    if (localStorage.getItem("list") === null) {
        localStorage.setItem("list", "[]");
    } else {
        list = JSON.parse(localStorage.getItem("list"));
        list.forEach((el,i) => {
            let e = $h(".elem", {"data-index": i, style: `top:${el.pos[0]};left: ${el.pos[1]};height: ${el.size[0]}px;width:${el.size[1]}px;padding: 5px;z-index: ${zindexCounter++};position:absolute`}, [
                el.name,
                $h("button", {"class":"btn","@click": e => (ev) => {
                    let x = e.parentElement.style.width;
                    e.parentElement.style.width = e.parentElement.style.height;
                    e.parentElement.style.height=x;
                    e.parentElement.children[5].childNodes[0].nodeValue = `${el.size[1]}cmx${el.size[0]}cm`;
                }}, ["Turn"]),
                $h("button", {"class":"btn","@click": e => (ev) => {list.splice(parseInt(e.getAttribute("data-index")),1);localStorage.setItem("list", JSON.stringify(list));e.parentElement.parentElement.removeChild(e.parentElement)}}, ["Delete"]),
                $h("small", {}, [
                    `(${el.pos[0]}:${el.pos[1]})`
                ]),
                " ",
                $h("small", {}, [`${el.size[0]/2}cmx${el.size[1]/2}cm`])

            ]);
            field.appendChild(e);
            dragElement(e);
        });
    }

    function createElement() {
        let e = $h(".elem", {style: `height: ${inputH.value*2}px;width:${inputW.value*2}px;padding: 5px;z-index: ${zindexCounter++};position:absolute`}, [
                inputN.value,
                $h("button", {"class":"btn","@click": e => (ev) => {let x = e.parentElement.style.width; e.parentElement.style.width = e.parentElement.style.height; e.parentElement.style.height=x;}}, ["Turn"]),
                $h("button", {"class":"btn","@click": e => (ev) => {list.splice(parseInt(e.getAttribute("data-index")),1);localStorage.setItem("list", JSON.stringify(list));e.parentElement.parentElement.removeChild(e.parentElement)}}, ["Delete"]),
                $h("small", {}, [
                    `(0px:0px)`
                ]),
                " ",
                $h("small", {}, [`${inputH.value}x${inputW.value}`])
        ]);
        field.appendChild(e);
        dragElement(e);

        e.setAttribute('data-index', list.push({element:e,pos:["0px","0px"],size:[inputH.value*2,inputW.value*2],name:inputN.value})-1);
        localStorage.setItem("list", JSON.stringify(list));
    }


function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    list[parseInt(elmnt.getAttribute("data-index"))].pos = [elmnt.style.top,elmnt.style.left];
    localStorage.setItem("list", JSON.stringify(list));
    elmnt.children[2].childNodes[0].nodeValue = `(${elmnt.style.top}:${elmnt.style.left})`
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}


});
